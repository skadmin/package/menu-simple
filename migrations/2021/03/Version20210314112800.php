<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314112800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'form.menu-simple.edit-item.article.placeholder', 'hash' => '89b9e012558c9f1846d4a2adf0176a52', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Začněte psát pro vyhledávání...', 'plural1' => '', 'plural2' => ''],
            ['original' => 'simple-menu-item-internal.index', 'hash' => '89dc3cccdd0dee68b3c6c88368842cf5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Úvodní stránka', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
