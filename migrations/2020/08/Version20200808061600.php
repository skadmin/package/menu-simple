<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200808061600 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'menu-simple.overview', 'hash' => '92ec2bd8bc6f87f3f12faaefc2710a0a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.menu-simple.title', 'hash' => '4ccc949c949c158da8b0daec2e5968c2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.menu-simple.description', 'hash' => '2dac741ac0ad98a36ff9e3602272fa50', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'menu-simple.overview.title', 'hash' => '01c7543dfd4fd1f10a6f629df888fbb2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Menu|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.action.inline-add', 'hash' => 'c7b1fbb305d1d3d48c677e281021ee45', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.name.req', 'hash' => '733201396f8121097334cc2ca5c78ea4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.is-active', 'hash' => '68c08049d5275828d81993841ecb0985', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.name', 'hash' => '4dcbf499f2f6f52a17e707158ab466ad', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.items-count', 'hash' => 'd3e6b7346688fc638111dbe20784f73b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet položek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.action.overview-items', 'hash' => '4a93771eb5dceff5a1a4a41d4170e6a4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.action.flash.inline-edit.success "%s"', 'hash' => 'f00c636a6301492cfc823ee0fe46dc30', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Menu "%s" byloúspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview.action.flash.inline-add.success "%s"', 'hash' => '8627e35a04b8c45d7d5b69478c3c2198', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Menu "%s" bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'menu-simple.overview-items.title - %s', 'hash' => 'a17f173974b208041f03f0e64c4d33ed', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Přehled položek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.action.new', 'hash' => '1e170b6e926ecc83321a7001aca806bf', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit položku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.name', 'hash' => 'b4d27bd7debdd8ea57d795a0a51584eb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.is-active', 'hash' => 'de9b65e6846f3259fdd2f96e7716240f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.action.subnew', 'hash' => '6653c43f6b0c4733336eb5d095db7c16', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vnořit položku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.action.edit', 'hash' => '8a312f6056f79b5c588ba0df923ddfd7', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.action.flash.sort.success', 'hash' => 'd110b16390b4ce8c372c60fd454795d3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí menu položky bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'menu-simple.edit-item.title', 'hash' => '2bb8ef8536f3c761594a390af2e31f13', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení položky menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.name', 'hash' => '01d795cc8915ee507351d107b783ca6f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.name.req', 'hash' => '4c40b453e70425b753a517e7d8f3e2e5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.menu-simple-item-type', 'hash' => '1b179e2d5e0a2cf42cd1405fbd88af44', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Typ položky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemBlind', 'hash' => '908f97e476dffae118ed5690965cd806', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Bez odkazu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemInternal', 'hash' => 'b19efaaab1dd096de3938c8a0ec4ab03', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Interní odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemExternal', 'hash' => '886d0c7bba08a1871df175107c5141f7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Externí odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemArticle', 'hash' => '6abab399941dacfc8658bfbb6d0f34cb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz na článek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.menu-simple-item-type.req', 'hash' => '998f5505456453ba92a99f60afb7391f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte prosím typ položky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.internal', 'hash' => '5274bfd96bde3d1c994731e4fd4588a2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Interní odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.external', 'hash' => 'b5fb1ab7552707b0fdd370eeefabcdb1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Externí odkaz', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.article', 'hash' => '9ad2bb4acaff994418598af555fe7fd5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Článek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.title', 'hash' => 'ddb60f0e5869d59dc96fae312ef45ebb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titulek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.target', 'hash' => 'd5ed0e4f368ff6381d4f15712f6d5781', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz otevřít', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.image-preview', 'hash' => '537637f6fed33fc00dfb27d9d0688d02', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obrázek položky menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.image-preview.rule-image', 'hash' => '8740ad58d9169ab0b19d1f3b903c2314', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.is-active', 'hash' => 'dbfbc7db9278407873f1102366c1218e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.send', 'hash' => '11ec09db06113a49ab2792ec40d62b53', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.send-back', 'hash' => '6bf42c12df6d480cd278f75b056e21d8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.back', 'hash' => 'b6c38525c85ff8f83320e3afa72908dd', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'menu-simple.edit-item.title - %s', 'hash' => 'd8719acbdb9fe70046d2262be269f942', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace položky menu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.flash.success.create', 'hash' => 'c077cacb19c773e52ec7f8f43e74e849', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položka menu byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.flash.success.update', 'hash' => '27d9ed42fda5523c617b6fb5614a690f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položka menu byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.menu-simple.edit-item.article.no-result', 'hash' => '48be8bc5eab53508ad9743efc25053b5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyly nalezeny žádné články: ', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
