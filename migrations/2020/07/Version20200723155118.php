<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200723155118 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE menu_simple (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu_simple_item (id INT AUTO_INCREMENT NOT NULL, menu_simple_item_id INT DEFAULT NULL, menu_simple_id INT DEFAULT NULL, value VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, sequence INT NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_D80659EB41ECB981 (menu_simple_item_id), INDEX IDX_D80659EBD1BA96A1 (menu_simple_id), INDEX menu_simple_item_type (type), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE menu_simple_item ADD CONSTRAINT FK_D80659EB41ECB981 FOREIGN KEY (menu_simple_item_id) REFERENCES menu_simple_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE menu_simple_item ADD CONSTRAINT FK_D80659EBD1BA96A1 FOREIGN KEY (menu_simple_id) REFERENCES menu_simple (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE menu_simple_item DROP FOREIGN KEY FK_D80659EBD1BA96A1');
        $this->addSql('ALTER TABLE menu_simple_item DROP FOREIGN KEY FK_D80659EB41ECB981');
        $this->addSql('DROP TABLE menu_simple');
        $this->addSql('DROP TABLE menu_simple_item');
    }
}
