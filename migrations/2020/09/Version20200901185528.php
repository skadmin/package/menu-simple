<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901185528 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'grid.menu-simple.overview-items.action.remove', 'hash' => '0b2ca6a6e9207a6b6daf19e097b8e5b3', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.action.remove %%s', 'hash' => '0f8a10c0f429002c66487705af874faa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odebrat položku menu "%%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.menu-simple.overview-items.action.flash.remove.success', 'hash' => 'd390a3923dc32f593d1210606122812d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položka menu byla úspěšně odebrána.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
