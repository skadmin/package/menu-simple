<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\MenuSimple\BaseControl;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimple;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimpleFacade;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\AMenuSimpleItem;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\ImageStorage\ImageStorage;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;
use function sprintf;

class OverviewItems extends GridControl
{
    use APackageControl;
    use IsActive;

    private MenuSimpleItemFacade $facade;
    private MenuSimple           $menuSimple;
    private LoaderFactory        $webLoader;
    private ImageStorage         $imageStorage;

    public function __construct(int $id, MenuSimpleFacade $facadeMenuSimple, MenuSimpleItemFacade $facade, Translator $translator, ImageStorage $imageStorage, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
        $this->webLoader    = $webLoader;

        $this->menuSimple = $facadeMenuSimple->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewItems.latte');
        $template->render();
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('menu-simple.overview-items.title - %s', $this->menuSimple->getName());
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->getItemsForWithParentItem());
        $grid->setTreeView([$this, 'getItemsForWithParentItem'], 'hasChildren');

        // COLUMNS
        $grid->addColumnText('name', 'grid.menu-simple.overview-items.name')
            ->setRenderer(function ($menuSimpleItem): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-item',
                        'id'      => $menuSimpleItem['id'],
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                if ($menuSimpleItem['image_preview']) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$menuSimpleItem['image_preview'], '60x60', 'shrink_only']);

                    $imagePreview = Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                        'class' => 'bg-transparent-image',
                    ]);

                    $name->addHtml($imagePreview)
                        ->addText(' ');
                }

                $name->addText($menuSimpleItem['name']);

                return $name;
            });
        $grid->addColumnText('is_active', 'grid.menu-simple.overview-items.is-active')
            ->setReplacement($this->getReplacementIsActive());

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('subnew', 'grid.menu-simple.overview-items.action.subnew', 'Component:default', ['parentMenuItemId' => 'id'])->addParameters([
                'package'      => new BaseControl(),
                'render'       => 'edit-item',
                'menuSimpleId' => $this->menuSimple->getId(),
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');

            $grid->addAction('edit', 'grid.menu-simple.overview-items.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-item',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $grid->addActionCallback('remove', 'grid.menu-simple.overview-items.action.remove', [$this, 'gridMenuSimpleOverviewItemRemove'])
                ->setIcon('trash')
                ->setTitle('grid.menu-simple.overview-items.action.remove')
                ->setConfirmation(new StringConfirmation('grid.menu-simple.overview-items.action.remove %%s', 'name'))
                ->setClass('btn btn-xs btn-outline-danger ajax-no');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.menu-simple.overview-items.action.new', [
                'package'      => new BaseControl(),
                'render'       => 'edit-item',
                'menuSimpleId' => $this->menuSimple->getId(),
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

//        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId = null, ?string $prevId = null, ?string $nextId = null, ?string $parentId = null): void
    {
        $currentItem = $this->facade->get(intval($itemId));
        $parentItem  = $parentId === null || $parentId === '' ? null : $this->facade->get(intval($parentId));

        if ($currentItem->getParentMenuItem() !== $parentItem) {
            $this->facade->updateParentMenuItem($currentItem, $parentItem);
        }

        $where = ['menu_simple_id = %d' => $currentItem->getMenu()->getId()];
        if (intval($parentId) > 0) {
            $where += ['menu_simple_item_id = %d' => $parentId];
        } else {
            $where += ['menu_simple_item_id IS NULL%s' => ''];
        }

        $this->facade->sort($itemId, $prevId, $nextId, $where);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.menu-simple.overview-items.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }

    /**
     * @return array<mixed>
     */
    public function getItemsForWithParentItem(?int $menuItemId = null): array
    {
        return $this->facade->getModelForMenuSimple($this->menuSimple, $menuItemId);
    }

    public function gridMenuSimpleOverviewItemRemove(string $menuItemID): void
    {
        $menuItem         = $this->facade->get(intval($menuItemID));
        $parentMenuItemId = null;
        $presenter        = $this->getPresenterIfExists();

        if (! $menuItem->isLoaded()) {
            if ($presenter !== null) {
                $presenter->flashMessage('grid.menu-simple.overview-items.action.flash.remove.fail', Flash::DANGER);
            }
        } else {
            $this->removeImagePreview($menuItem);
            $this->facade->remove($menuItem);

            if ($presenter !== null) {
                $presenter->flashMessage('grid.menu-simple.overview-items.action.flash.remove.success', Flash::SUCCESS);
            }
        }

        $this['grid']->reload();
    }

    private function removeImagePreview(AMenuSimpleItem $menuItem): void
    {
        if ($menuItem->getImagePreview() !== null) {
            $this->imageStorage->delete($menuItem->getImagePreview());
        }

        foreach ($menuItem->getMenuItems() as $childMenuItem) {
            $this->removeImagePreview($childMenuItem);
        }
    }
}
