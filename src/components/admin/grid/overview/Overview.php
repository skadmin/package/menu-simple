<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\MenuSimple\BaseControl;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimple;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimpleFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function boolval;
use function intval;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private MenuSimpleFacade $facade;
    public LoaderFactory     $webLoader;

    public function __construct(MenuSimpleFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'menu-simple.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.menu-simple.overview.name')
            ->setRenderer(function (MenuSimple $menuSimple): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'overview-items',
                        'id'      => $menuSimple->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($menuSimple->getName());

                if ($this->loggedUser->isInRole('master-admin')) {
                    $code = Html::el('code', ['class' => 'text-muted small'])->setText($menuSimple->getCode());
                    $name->addHtml('<br>')
                        ->addHtml($code);
                }

                return $name;
            });
        $this->addColumnIsActive($grid, 'menu-simple.overview');
        $grid->addColumnText('items-count', 'grid.menu-simple.overview.items-count')
            ->setRenderer(static function (MenuSimple $menuSimple): int {
                return $menuSimple->getItems()->count();
            })->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.menu-simple.overview.name');
        $this->addFilterIsActive($grid, 'menu-simple.overview');

        // ACTION
        $grid->addAction('overviewItems', 'grid.menu-simple.overview.action.overview-items', 'Component:default', ['id' => 'id'])
            ->addParameters([
                'package' => new BaseControl(),
                'render'  => 'overview-items',
            ])->setIcon('bars')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.menu-simple.overview.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];
        }

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.menu-simple.overview.name')
            ->setRequired('grid.menu-simple.overview.name.req');
        $container->addSelect('isActive', 'grid.menu-simple.overview.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $menuSimple = $this->facade->create($values->name, boolval($values->isActive));

        $message = new SimpleTranslation('grid.menu-simple.overview.action.flash.inline-add.success "%s"', [$menuSimple->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $container->addText('name', 'grid.menu-simple.overview.name')
            ->setRequired('grid.menu-simple.overview.name.req');
        $container->addSelect('isActive', 'grid.menu-simple.overview.is-active', Constant::DIAL_YES_NO);
    }

    public function onInlineEditDefaults(Container $container, MenuSimple $menuSimple): void
    {
        $container->setDefaults([
            'name'     => $menuSimple->getName(),
            'isActive' => intval($menuSimple->isActive()),
        ]);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit(int|string $id, ArrayHash $values): void
    {
        $menuSimple = $this->facade->update(intval($id), $values->name, boolval($values->isActive));

        $message = new SimpleTranslation('grid.menu-simple.overview.action.flash.inline-edit.success "%s"', [$menuSimple->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }
}
