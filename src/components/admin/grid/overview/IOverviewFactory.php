<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
