<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Components\Admin;

use SkadminUtils\FormControls\UI\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Exception;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use ReflectionClass;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use Skadmin\MenuSimple\BaseControl;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimple;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimpleFacade;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\AMenuSimpleItem;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemFacade;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemArticle;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function assert;
use function count;
use function explode;
use function intval;
use function is_string;
use function preg_replace;
use function sprintf;
use function str_replace;

class EditItem extends FormWithUserControl
{
    use APackageControl;

    public LoaderFactory         $webLoader;
    private MenuSimpleItemFacade $facade;
    private MenuSimpleFacade     $facadeMenuSimple;
    private ArticleFacade        $facadeArticle;
    private ImageStorage         $imageStorage;
    private AMenuSimpleItem      $menuSimpleItem;
    private ?MenuSimple          $menuSimple     = null;
    private ?AMenuSimpleItem     $parentMenuItem = null;

    public function __construct(?int $id, MenuSimpleItemFacade $facade, MenuSimpleFacade $facadeMenuSimple, ArticleFacade $facadeArticle, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade           = $facade;
        $this->facadeMenuSimple = $facadeMenuSimple;
        $this->facadeArticle    = $facadeArticle;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->menuSimpleItem = $this->facade->get($id);

        if ($this->menuSimpleItem->isLoaded()) {
            $this->menuSimple = $this->menuSimpleItem->getMenu();
        } elseif (isset($_GET['menuSimpleId'])) {
            $this->menuSimple = $this->facadeMenuSimple->get(intval($_GET['menuSimpleId']));
        }

        if ($this->menuSimpleItem->isLoaded()) {
            $this->parentMenuItem = $this->menuSimpleItem->getParentMenuItem();
        } elseif (isset($_GET['parentMenuItemId'])) {
            $this->parentMenuItem = $this->facade->get(intval($_GET['parentMenuItemId']));
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->menuSimpleItem->isLoaded()) {
            return new SimpleTranslation('menu-simple.edit-item.title - %s', $this->menuSimpleItem->getName());
        }

        return 'menu-simple.edit-item.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // připravíme hodnotu
        $type      = $values->menuSimpleItemType;
        $value     = $values->menuSipleItemTypes->{$type}; //@phpstan-ignore-line
        $typeClass = $this->generateMenuItemType()[$type];

        if ($type === MenuSimpleItemArticle::TYPE) {
            $value = $this->facadeArticle->get($value);
        }

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->menuSimpleItem->isLoaded()) {
            if ($this->menuSimple === null) {
                throw new Exception('Menu simple is not set!');
            }

            if ($identifier !== null && $this->menuSimpleItem->getImagePreview() !== null) {
                $this->imageStorage->delete($this->menuSimpleItem->getImagePreview());
            }

            $this->menuSimpleItem = $this->facade->update(
                $this->menuSimpleItem->getId(),
                $this->menuSimple,
                $this->parentMenuItem,
                $typeClass,
                $value,
                $values->title,
                $values->target,
                $values->name,
                $values->isActive,
                $identifier
            );
            $this->onFlashmessage('form.menu-simple.edit-item.flash.success.update', Flash::SUCCESS);
        } else {
            $this->menuSimpleItem = $this->facade->create(
                $this->facadeMenuSimple->get(intval($values->menuSimpleId)),
                $values->parentMenuItemId ? $this->facade->get(intval($values->parentMenuItemId)) : null,
                $typeClass,
                $value,
                $values->title,
                $values->target,
                $values->name,
                $values->isActive,
                $identifier
            );
            $this->onFlashmessage('form.menu-simple.edit-item.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-item',
            'id'      => $this->menuSimpleItem->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-items',
            'id'      => $_POST['menuSimple_id'] ?? $this->menuSimpleItem->getMenu()->getId(),
        ]);
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editItem.latte');

        $template->imageStorage   = $this->imageStorage;
        $template->menuSimpleItem = $this->menuSimpleItem;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $menuSimpleItemTypes = $this->generateMenuItemType();

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        if (! $this->menuSimpleItem->isLoaded()) {
            $form->addHidden('menuSimpleId', $this->menuSimple !== null ? (string) $this->menuSimple->getId() : '');
            $form->addHidden('parentMenuItemId', $this->parentMenuItem !== null ? (string) $this->parentMenuItem->getId() : '');
        }

        // INPUT
        $form->addText('name', 'form.menu-simple.edit-item.name')
            ->setRequired('form.menu-simple.edit-item.name.req');
        $form->addCheckbox('isActive', 'form.menu-simple.edit-item.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('imagePreview', 'form.menu-simple.edit-item.image-preview');

        $form->addText('title', 'form.menu-simple.edit-item.title');
        $form->addSelect('target', 'form.menu-simple.edit-item.target', Constant::HREF_TARGETS)
            ->setDefaultValue(Constant::HREF_TARGET_SELF)
            ->setTranslator(null);

        // TYPE
        $inputMenuSimpleItemType = $form->addSelect('menuSimpleItemType', 'form.menu-simple.edit-item.menu-simple-item-type', $menuSimpleItemTypes)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('form.menu-simple.edit-item.menu-simple-item-type.req');

        $cMenuSipleItemTypes = $form->addContainer('menuSipleItemTypes');
        foreach ($menuSimpleItemTypes as $name => $menuSimpleItemType) {
            if ($menuSimpleItemType === MenuSimpleItemArticle::class) {
                $menuSimpleItemType::setArticleFacade($this->facadeArticle);
            }

            $menuSimpleItemType::setInput($cMenuSipleItemTypes, $name, sprintf('form.menu-simple.edit-item.%s', Strings::webalize($name)));

            $inputMenuSimpleItemType->addCondition(Form::EQUAL, $name)
                ->toggle(sprintf('menu-simple-item-type-%s', $name))
                ->endCondition();
        }

        // BUTTON
        $form->addSubmit('send', 'form.menu-simple.edit-item.send');
        $form->addSubmit('sendBack', 'form.menu-simple.edit-item.send-back');
        $form->addSubmit('back', 'form.menu-simple.edit-item.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return array<string>
     */
    private function generateMenuItemType(): array
    {
        foreach ((new ReflectionClass(AMenuSimpleItem::class))->getAttributes() as $attribute) {
            if ($attribute->getName() === 'Doctrine\ORM\Mapping\DiscriminatorMap') {
                return $attribute->getArguments()[0];
            }
        }
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->menuSimpleItem->isLoaded()) {
            return [];
        }

        $value = $this->menuSimpleItem->getValueData() ?? null;

        return [
            'name'               => $this->menuSimpleItem->getName(),
            'title'              => $this->menuSimpleItem->getTitle(),
            'target'             => $this->menuSimpleItem->getTarget(),
            'menuSimpleItemType' => $this->menuSimpleItem->getType(),
            'menuSipleItemTypes' => [$this->menuSimpleItem->getType() => $value],
            'isActive'           => $this->menuSimpleItem->isActive(),
        ];
    }
}
