<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Components\Admin;

interface IEditItemFactory
{
    public function create(?int $id = null): EditItem;
}
