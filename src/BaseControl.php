<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'menu-simple';
    public const DIR_IMAGE = 'menu-simple';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-bars']),
            'items'   => ['overview'],
        ]);
    }
}
