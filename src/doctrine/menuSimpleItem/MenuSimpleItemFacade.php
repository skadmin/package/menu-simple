<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimpleItem;

use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimple;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemBlind;
use SkadminUtils\DoctrineTraits;

use function assert;
use function sprintf;

final class MenuSimpleItemFacade extends Facade
{
    use DoctrineTraits\Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = AMenuSimpleItem::class;
    }

    public function create(MenuSimple $menuSimple, ?AMenuSimpleItem $parentMenuItem, string $typeClass, mixed $value, string $title, string $target, string $name, bool $isActive, ?string $imagePreview): AMenuSimpleItem
    {
        return $this->update(null, $menuSimple, $parentMenuItem, $typeClass, $value, $title, $target, $name, $isActive, $imagePreview);
    }

    public function update(?int $id, MenuSimple $menuSimple, ?AMenuSimpleItem $parentMenuItem, string $typeClass, mixed $value, string $title, string $target, string $name, bool $isActive, ?string $imagePreview): AMenuSimpleItem
    {
        $menuItem = $this->get($id, $typeClass);
        $menuItem->update($menuSimple, $parentMenuItem, $value, $title, $target, $name, $isActive, $imagePreview);

        if (! $menuItem->isLoaded()) {
            $menuItem->setSequence($this->getValidSequence());
        }

        $this->em->persist($menuItem);
        $this->em->flush();

        return $menuItem;
    }

    public function updateParentMenuItem(AMenuSimpleItem $menuItem, ?AMenuSimpleItem $parentMenuItem): AMenuSimpleItem
    {
        $menuItem->updateParentMenuItem($parentMenuItem);
        $this->em->persist($menuItem);
        $this->em->flush();

        return $menuItem;
    }

    public function remove(AMenuSimpleItem $menuItem): void
    {
        $this->em->remove($menuItem);
        $this->em->flush();
    }

    public function get(?int $id = null, ?string $typeClass = null): AMenuSimpleItem
    {
        if ($id === null) {
            if ($typeClass === null) {
                return new MenuSimpleItemBlind();
            }

            return new $typeClass();
        }

        $menuItem = parent::get($id);
        assert($menuItem instanceof AMenuSimpleItem || $menuItem === null);

        if ($menuItem === null) {
            if ($typeClass === null) {
                return new MenuSimpleItemBlind();
            }

            return new $typeClass();
        }

        if ($typeClass !== null && $menuItem::class !== $typeClass) {
            $newMenuItem = $menuItem->changeType($typeClass);
            $this->em->remove($menuItem);
            $menuItem = $newMenuItem;
        }

        return $menuItem;
    }

    /**
     * @return AMenuSimpleItem[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }

    /**
     * @return array<mixed>
     */
    public function getModelForMenuSimple(MenuSimple $menuSimple, ?int $menuItemId): array
    {
        $sql = sprintf('
            SELECT
                b.*, c.count AS hasChildren
            FROM
                menu_simple_item b
            LEFT JOIN (
                SELECT
                    COUNT(a.id) AS count,
                    a.menu_simple_item_id AS menu_simple_item_id
                FROM
                    menu_simple_item a
                GROUP BY
                    menu_simple_item_id
                ) c ON
                    c.menu_simple_item_id = b.id
            WHERE
                b.menu_simple_item_id %s
                AND b.menu_simple_id = %s
            ORDER BY b.sequence ASC, b.id ASC
        ', $menuItemId === null ? 'is null' : '= ' . $menuItemId, $menuSimple->getId());

        $stmt = $this->em
            ->getConnection()
            ->prepare($sql);
        $result = $stmt->execute();

        return $result->fetchAll();

//        /** @var EntityRepository $repository */
//        $repository = $this->em
//            ->getRepository($this->table);
//
//        $criteria = Criteria::create();
//        $criteria->where(Criteria::expr()->eq('menuSimple', $menuSimple));
//        $criteria->andWhere(Criteria::expr()->eq('menuSimpleItem', $menuItemId));
//
//        return $repository->createQueryBuilder('a')
//            ->orderBy('a.sequence')
//            ->addCriteria($criteria);
    }
}
