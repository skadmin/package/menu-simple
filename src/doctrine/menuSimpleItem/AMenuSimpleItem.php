<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimpleItem;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Types\Types;
use Nette\Forms\Container;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimple;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemArticle;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemBlind;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemExternal;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType\MenuSimpleItemInternal;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

use function assert;
use function intval;

#[ORM\Entity]
#[ORM\Table(name: 'menu_simple_item')]
#[ORM\Index(name: 'menu_simple_item_type', columns: ['type'])]
#[ORM\InheritanceType(value: 'SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap([
    MenuSimpleItemBlind::TYPE => MenuSimpleItemBlind::class,
    MenuSimpleItemInternal::TYPE => MenuSimpleItemInternal::class,
    MenuSimpleItemExternal::TYPE => MenuSimpleItemExternal::class,
    MenuSimpleItemArticle::TYPE => MenuSimpleItemArticle::class,
])]
#[ORM\HasLifecycleCallbacks]
abstract class AMenuSimpleItem
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;

    #[ORM\Column]
    protected string $value = '';

    #[ORM\Column(type: Types::TEXT)]
    protected string $title = '';

    #[ORM\Column(nullable: true)]
    protected ?string $target = '';

    #[ORM\ManyToOne(targetEntity: AMenuSimpleItem::class, inversedBy: 'menuSimpleItems')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?AMenuSimpleItem $menuSimpleItem = null;

    /** @var ArrayCollection|Collection|AMenuSimpleItem[] */
    #[ORM\OneToMany(targetEntity: AMenuSimpleItem::class, mappedBy: 'menuSimpleItem')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private ArrayCollection|Collection|array $menuSimpleItems;

    #[ORM\ManyToOne(targetEntity: MenuSimple::class, inversedBy: 'items')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private MenuSimple $menuSimple;

    public function update(MenuSimple $menuSimple, ?AMenuSimpleItem $menuSimpleItem, mixed $value, string $title, ?string $target, string $name, bool $isActive, ?string $imagePreview): void
    {
        $this->menuSimple     = $menuSimple;
        $this->menuSimpleItem = $menuSimpleItem;

        $this->value  = $value;
        $this->title  = $title;
        $this->target = $target;

        $this->name     = $name;
        $this->isActive = $isActive;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function setNewTypeProperties(MenuSimple $menuSimple, ?AMenuSimpleItem $menuSimpleItem, string $title, ?string $target, string $name, bool $isActive, int $sequence, ?string $imagePreview): void
    {
        $this->menuSimple     = $menuSimple;
        $this->menuSimpleItem = $menuSimpleItem;

        $this->name   = $name;
        $this->title  = $title;
        $this->target = $target;

        $this->isActive = $isActive;
        $this->sequence = $sequence;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    public function getValueData(): mixed
    {
        return $this->value;
    }

    public function updateParentMenuItem(?AMenuSimpleItem $parentMenuItem): void
    {
        $this->menuSimpleItem = $parentMenuItem;
    }

    public function getParentMenuItem(): ?AMenuSimpleItem
    {
        return $this->menuSimpleItem;
    }

    /**
     * @return ArrayCollection|Collection|AMenuSimpleItem[]
     */
    public function getMenuItems(bool $onlyActive = false)
    {
        if (! $onlyActive) {
            return $this->menuSimpleItems;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isActive', true));

        return $this->menuSimpleItems->matching($criteria);
    }

    public function getMenu(): MenuSimple
    {
        return $this->menuSimple;
    }

    abstract public function getType(): string;

    abstract public static function setInput(Container &$form, string $name, string $label): void;

    public function changeType(string $destination): AMenuSimpleItem
    {
        $newMenuSimpleItem = new $destination();
        assert($newMenuSimpleItem instanceof AMenuSimpleItem);

        $newMenuSimpleItem->setNewTypeProperties(
            $this->getMenu(),
            $this->getParentMenuItem(),
            $this->getTitle(),
            $this->getTarget(),
            $this->getName(),
            $this->isActive(),
            intval($this->getSequence()),
            $this->getImagePreview()
        );

        foreach ($this->getMenuItems() as $menuSimpleItem) {
            $menuSimpleItem->updateParentMenuItem($newMenuSimpleItem);
        }

        return $newMenuSimpleItem;
    }
}
