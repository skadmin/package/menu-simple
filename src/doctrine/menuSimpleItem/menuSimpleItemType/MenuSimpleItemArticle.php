<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType;

use Nette\Forms\Container;
use Nette\Localization\ITranslator;
use Skadmin\Article\Doctrine\Article\Article;
use Skadmin\Article\Doctrine\Article\ArticleFacade;
use Skadmin\MenuSimple\Doctrine\MenuSimple\MenuSimple;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\AMenuSimpleItem;
use Skadmin\MenuSimple\Exception\ExceptionMenuSimple;
use Doctrine\ORM\Mapping as ORM;

use function sprintf;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class MenuSimpleItemArticle extends AMenuSimpleItem
{
    public const TYPE = 'article';

    private static ?ArticleFacade $facade = null;

    #[ORM\ManyToOne(targetEntity: Article::class)]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    protected Article $article;

    public function update(MenuSimple $menuSimple, ?AMenuSimpleItem $menuSimpleItem, mixed $value, string $title, ?string $target, string $name, bool $isActive, ?string $imagePreview): void
    {
        parent::update($menuSimple, $menuSimpleItem, (string) $value->getId(), $title, $target, $name, $isActive, $imagePreview);
        $this->article = $value;
    }

    public function getValue(): Article
    {
        return $this->article;
    }

    public function getValueData(): mixed
    {
        return $this->article->getId();
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public static function setInput(Container &$form, string $name, string $label): void
    {
        if (self::$facade === null) {
            throw  new ExceptionMenuSimple('Article facade is not set!');
        }

        $articles = [];
        foreach (self::$facade->getAll(false, false) as $article) {
            $articles[$article->getId()] = $article->getName();
        }

        $inputSelect = $form->addSelect($name, $label, $articles)
            ->setHtmlAttribute('class', 'form-control chosen-select');

        $dataChosenPlaceholderText = sprintf('%s.placeholder', $label);
        $dataChosenNoResultText    = sprintf('%s.no-result', $label);

        $translator = $inputSelect->getTranslator();
        if ($translator instanceof ITranslator) {
            $inputSelect->setTranslator(null);
            $inputSelect->setCaption($label);

            $dataChosenPlaceholderText = $translator->translate($dataChosenPlaceholderText);
            $dataChosenNoResultText    = $translator->translate($dataChosenNoResultText);
        }

        $inputSelect
            ->setHtmlAttribute('data-chosen-placeholder-text', $dataChosenPlaceholderText)
            ->setHtmlAttribute('data-chosen-no-result-text', $dataChosenNoResultText);
    }

    public static function setArticleFacade(ArticleFacade $facade): void
    {
        self::$facade = $facade;
    }
}
