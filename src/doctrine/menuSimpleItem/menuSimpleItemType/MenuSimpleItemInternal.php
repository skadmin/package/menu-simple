<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType;

use App\Model\System\Constant;
use App\Model\System\Settings;
use Nette\Forms\Container;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\AMenuSimpleItem;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class MenuSimpleItemInternal extends AMenuSimpleItem
{
    public const TYPE = 'internal';

    public function getType(): string
    {
        return self::TYPE;
    }

    public static function setInput(Container &$form, string $name, string $label): void
    {
        $form->addSelect($name, $label, Settings::MENU_SIMPLE_ITEM_INTERNAL)
            ->setHtmlAttribute('class', 'form-control')
            ->setPrompt(Constant::PROMTP);
    }
}
