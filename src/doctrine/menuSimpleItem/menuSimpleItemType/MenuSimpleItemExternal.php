<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimpleItem\MenuSimpleItemType;

use Nette\Forms\Container;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\AMenuSimpleItem;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class MenuSimpleItemExternal extends AMenuSimpleItem
{
    public const TYPE = 'external';

    public function getType(): string
    {
        return self::TYPE;
    }

    public static function setInput(Container &$form, string $name, string $label): void
    {
        $form->addText($name, $label)
            ->setHtmlAttribute('class', 'form-control');
    }
}
