<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimple;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Skadmin\MenuSimple\Doctrine\MenuSimpleItem\AMenuSimpleItem;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class MenuSimple
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Code;
    use Entity\IsActive;

    /** @var Collection|AMenuSimpleItem[] */
    #[ORM\OneToMany(targetEntity: AMenuSimpleItem::class, mappedBy: 'menuSimple')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private Collection|array $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function update(string $name, bool $isActive): void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
    }

    /**
     * @return ArrayCollection|Collection|AMenuSimpleItem[]
     */
    public function getItems(bool $onlyActive = false): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        $criteria->where(Criteria::expr()->isNull('menuSimpleItem'));

        if ($onlyActive) {
            $criteria->andWhere(Criteria::expr()->eq('isActive', true));
        }

        return $this->items->matching($criteria);
    }
}
