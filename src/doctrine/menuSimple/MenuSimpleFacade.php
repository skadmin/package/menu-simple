<?php

declare(strict_types=1);

namespace Skadmin\MenuSimple\Doctrine\MenuSimple;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;

final class MenuSimpleFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = MenuSimple::class;
    }

    public function create(string $name, bool $isActive): MenuSimple
    {
        return $this->update(null, $name, $isActive);
    }

    public function update(?int $id, string $name, bool $isActive): MenuSimple
    {
        $menuSimple = $this->get($id);
        $menuSimple->update($name, $isActive);

        $this->em->persist($menuSimple);
        $this->em->flush();

        return $menuSimple;
    }

    public function get(?int $id = null): MenuSimple
    {
        if ($id === null) {
            return new MenuSimple();
        }

        $menuSimple = parent::get($id);

        if ($menuSimple === null) {
            return new MenuSimple();
        }

        return $menuSimple;
    }

    public function findByCode(string $code): ?MenuSimple
    {
        $criteria = [
            'code'     => $code,
            'isActive' => true,
        ];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
